ruby-appraisal (2.5.0-1) UNRELEASED; urgency=medium

  [ James Valleroy ]
  * Use https:// for debian/watch
  * Use https:// for Homepage
  * Use https:// for copyright Format and Source
  * Update Vcs-* fields for Salsa

  [ Andrew Lee (李健秋) ]
  * debian/watch: fetch tarball from upstream's github instead.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Andreas Tille ]
  * New upstream version 2.5.0
    Closes: #882278
  * d/watch: Only report released versions (no beta, no rc)
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Drop deprecated {XS,XB}-Ruby-Versions fields (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Remove overrides for lintian tags that are no longer supported: no-upstream-changelog
  * watch file standard 4 (routine-update)
  * Build-Depends: ruby-activesupport <!nocheck>
  * Less offensive short description

 -- Andreas Tille <tille@debian.org>  Wed, 13 Nov 2024 08:56:02 +0100

ruby-appraisal (0.5.1-2) unstable; urgency=medium

  [ Sebastien Badia ]
  * Team upload
  * d/compat: Bump compat version to 9 and wrap file
  * d/control: Update Vcs-* fields (use cgit and https)
  * Bump Standards-Versions to 3.9.7 (no changes)
  * Update debian packaging

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Run wrap-and-sort on packaging files

  [ Andrew Lee (李健秋) ]
  * New upload to support current Ruby version. (Closes: #830060)
  * Bump standard version to 3.9.8.

 -- Andrew Lee (李健秋) <andrew.lee@collabora.co.uk>  Fri, 26 Aug 2016 15:35:13 +0800

ruby-appraisal (0.5.1-1) unstable; urgency=low

  * Initial release (Closes: #702343)

 -- Praveen Arimbrathodiyil <praveen@debian.org>  Tue, 05 Mar 2013 18:29:18 +0530
